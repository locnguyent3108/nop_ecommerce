package commons;

import listener.Log;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.*;
import org.testng.Assert;

import java.time.Duration;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import static commons.GlobalConstant.LONG_TIMEOUT;
import static commons.GlobalConstant.SHORT_TIMEOUT;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalToIgnoringCase;

public class BasePage {
    WebDriver driver;
    WebDriverWait wait;
    Wait<WebDriver> fluentWait;
    WebElement e;
    Select select;
    Actions act;
    JavascriptExecutor jsExec;
    public BasePage(WebDriver driver) {
        this.driver = driver;
    }

    public WebDriver getDriver() {
        return driver;
    }

    //=================== ACTIONS ===================
    //----- get element
    private By byXpath(String locator) {
        return By.xpath(locator);
    }
    public WebElement findElement(String locator) {
        return driver.findElement(byXpath(locator));
    }

    public List<WebElement> findElements(String locator) {
        return driver.findElements(byXpath(locator));
    }

    public Integer countElement(String locator) {
        List<WebElement> le = findElements(locator);
        return le.size();
    }

    //------ interact with browser
    public void openUrl(String urlValue) {
        driver.get(urlValue);
    }

    public String getPageTitle() {
        return driver.getTitle();
    }

    public void back() {
        driver.navigate().back();
    }

    public void refresh() {
        driver.navigate().refresh();
    }

    public void forward() {
        driver.navigate().forward();
    }

    //--------Alert
    public void acceptAlert() {
        driver.switchTo().alert().accept();
    }

    public void cancelAlert() {
        driver.switchTo().alert().dismiss();
    }

    public void sendkeyToAlert(String value) {
        driver.switchTo().alert().sendKeys(value);
    }

    public String getAlertText() {
        return driver.switchTo().alert().getText();
    }

    //--------browsers and window
    public void switchWindowByID(String parentID) {
        Set<String> listWindow = driver.getWindowHandles();
        for (String window: listWindow) {
            if(window.equalsIgnoreCase(parentID)) {
                driver.switchTo().window(window);
                break;
            }
        }
    }

    public void switchWindowByTitle(String title) {
        Set<String> allWindows = driver.getWindowHandles();
        for (String runWindow : allWindows) {
            driver.switchTo().window(runWindow);
            String currentWinTitle = driver.getTitle();
            if (currentWinTitle.equals(title)) {
                break;
            }
        }
    }

    //--------built in fuction
    public void click(String locator) {
        findElement(locator).click();
    }

    public void input(String locator, String value) {
        e = findElement(locator);
        e.sendKeys(value);
    }

    public boolean isSelected(WebElement e) {
        return e.isSelected();
    }

    public void tickToCheckBox(String locator) {
        WebElement e = findElement(locator);
        if (isSelected(e) == true) {
            e.click();
        }
    }

    public void unTickToCheckBox(String locator) {
        e = findElement(locator);
        if(isSelected(e) == false) {
            e.click();
        }
    }




    public String getTextElement(String locator) {
        return findElement(locator).getText();
    }

    public String getElementAttribute(String locator, String attributeName) {
        return findElement(locator).getAttribute(attributeName);
    }
    public void clearText(String locator) {
        e = findElement(locator);
        e.clear();
    }
    public void selectValueInDropdown(String locator, String value) {
        select = new Select(findElement(locator));
        select.selectByVisibleText(value);
    }

    public String getSelectedItemInDropdown(String locator) {
        select = new Select(findElement(locator));
        return select.getFirstSelectedOption().getText();
    }

    public void selectItemInCustomDropdown(String parentXPath, String allItemXpath, String expectedValueItem) throws InterruptedException {
        WebElement parentNode = findElement(parentXPath);
        JavascriptExecutor jsExec = (JavascriptExecutor) driver;
        jsExec.executeScript("arguments[0].click", parentNode);

        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(byXpath(allItemXpath)));
        List<WebElement> allItems = findElements(allItemXpath);

        for (WebElement childElement : allItems) {
            if (childElement.getText().equals(expectedValueItem)) {
                childElement.click();
            } else {
                jsExec.executeScript("arguments[0].scrollToView(true)", childElement);
                jsExec.executeScript("arguments[0].click()", childElement);
            }
            break;
        }
    }

    public boolean isElementDisplayed(String locator) {
        try {
            driver.manage().timeouts().implicitlyWait(SHORT_TIMEOUT, TimeUnit.SECONDS);
            e = findElement(locator);
            driver.manage().timeouts().implicitlyWait(LONG_TIMEOUT, TimeUnit.SECONDS);
            return e.isDisplayed();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean isElementEnabled(String locator) {
        try {
            driver.manage().timeouts().implicitlyWait(SHORT_TIMEOUT, TimeUnit.SECONDS);
            e = findElement(locator);
            driver.manage().timeouts().implicitlyWait(LONG_TIMEOUT, TimeUnit.SECONDS);
            return e.isEnabled();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public void switchToFrame(String locator) {
        driver.switchTo().frame(findElement(locator));
        Log.info("Frame id: " + driver.getWindowHandle());
    }

    public void switchToDefaultContent() {
        driver.switchTo().defaultContent();
    }

    public void hoverMouseToElement(String locator) {
        act = new Actions(driver);
        act.doubleClick(findElement(locator))
            .perform();
    }

    public void rightClick(String locator) {
        act = new Actions(driver);
        act.contextClick(findElement(locator))
            .perform();
    }

    //JAVASCRIPT EXECUTOR
    public Object executeForeBrowser(String js) {
        jsExec = (JavascriptExecutor) driver;
        return jsExec.executeScript(js);
    }

    public void scrollToBottomPage() {
        jsExec.executeScript("window.scrollBy(0,document.bodt.scrollHeight)");
    }

    public void scrollToElement(WebElement element) {
        jsExec.executeScript("window.scrollBy(arguments[0].scrollIntoView();",element);
    }

    public void clickByJs(String locator) {
        jsExec.executeScript("arguments[0].click()");
    }

    public boolean isImageLoaded(String locator) {
        jsExec = (JavascriptExecutor) driver;
        WebElement e = findElement(locator);
        boolean status = (Boolean) jsExec.executeScript
                ("return arguments[0].complete " +
                        "&& typeof arguments[0].naturalWidth != \"undefined\" " +
                        "&& arguments[0].naturalWidth > 0", e);
        Boolean result = (status) ? true : false;
        return result;
    }

    //WAIT
    public void fluentWaitConfig() {
        fluentWait = new FluentWait<>(driver).withTimeout(Duration.ofSeconds(10))
        .pollingEvery(Duration.ofSeconds(2))
        .withMessage("Looking for Element")
                .ignoring(NoSuchElementException.class);
    }
    public void waitForElementVisible(String locator) {
        wait = new WebDriverWait(driver, LONG_TIMEOUT);
//        wait.until(ExpectedConditions.visibilityOf(findElement(locator)));
        wait.until(ExpectedConditions.visibilityOfElementLocated(byXpath(locator)));
    }

    public void waitForPageReady() throws InterruptedException {
         FluentWait wait = new WebDriverWait(driver, LONG_TIMEOUT);
         Thread.sleep(1000);
         wait.until(( webDriver -> ((JavascriptExecutor) webDriver)
                 .executeScript("return document.readyState").equals("complete")));


    }

    public void waitForElementInvisible(String locator, String value) {
        wait = new WebDriverWait(driver, LONG_TIMEOUT);
        wait.until(ExpectedConditions.invisibilityOfElementWithText(byXpath(locator), value));
    }

    public void waitForElementClickable(String locator) {
        wait = new WebDriverWait(driver, LONG_TIMEOUT);
        wait.until(ExpectedConditions.elementToBeClickable(byXpath(locator)));
    }

    public boolean verifyTrue(boolean condition) {
        try {
            Assert.assertTrue(condition);
            return true;
        } catch (Throwable e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean verifyFalse(boolean condition) {
        try {
            Assert.assertFalse(condition);
            return true;
        } catch (Throwable e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean verifyEqual(Object actual, Object expected) {
        try {
            Assert.assertEquals(actual, expected);
            return true;
        } catch (Throwable e) {
            e.printStackTrace();
            return false;
        }
    }

    public void sameTextWith(WebElement e, String expectedText) {
        assertThat(e.getText(), equalToIgnoringCase(expectedText));
    }
}

