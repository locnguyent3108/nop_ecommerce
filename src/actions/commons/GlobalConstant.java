package commons;

public class GlobalConstant {
    public static final int LONG_TIMEOUT = 20;
    public static final int SHORT_TIMEOUT = 5;
    public static final String HOME_URL = "https://demo.nopcommerce.com/";
    public static final String EMAIL = "automationfc.vn@gmail.com";
    public static final String PASSWORD = "123456";
}
