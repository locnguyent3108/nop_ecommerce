package commons;

import org.openqa.selenium.WebDriver;
import pageObjects.*;

public class PageGenerator {
    public static HomePageObject getHomePage(WebDriver driver) {
        return new HomePageObject(driver);
    }

    public static Object getPageInstance(WebDriver driver, String pageName) {
        switch (pageName.toLowerCase()) {
            case "register":
                return new RegisterPageObject(driver);
            case "login":
                return new LoginPageObject(driver);
            case "myaccount":
                return new MyAccountPageObject(driver);
            case "product":
                return new ProductPageObject(driver);
            default:
                return new HomePageObject(driver);
        }
    }
}
