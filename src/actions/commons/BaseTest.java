package commons;

import io.github.bonigarcia.wdm.WebDriverManager;
import listener.Log;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import pageObjects.HomePageObject;

public class BaseTest {
    WebDriver driver;

    public WebDriver getDriver() {
        return driver;
    }

    @BeforeClass
    public void initTest() {
        Log.info("Setup Driver to test");
        driver = getBrowserDriver();
        Log.info("Driver ID: " + driver.toString());
        driver.manage().window().maximize();
    }

    @AfterClass
    public void tearDown(ITestContext result) {
        Log.info("Finish Test case" + result.getAllTestMethods().toString());
        driver.quit();
    }

    public WebDriver getBrowserDriver() {
        WebDriverManager.firefoxdriver().setup();
        driver = new FirefoxDriver();
        System.out.println("Driver ID: " + driver.toString());
        return driver;
    }
}
