package pageObjects;

import commons.BasePage;
import org.openqa.selenium.WebDriver;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.text.IsEqualIgnoringCase.equalToIgnoringCase;
import static pageUI.MyAccountPageUI.*;

public class MyAccountPageObject extends BasePage {
    public MyAccountPageObject(WebDriver driver) {
        super(driver);
    }

    public void updateFirstName(String updateFirstName) {
        waitForElementVisible(FIRSTNAME_TEXTFIELD);
        clearText(FIRSTNAME_TEXTFIELD);

        input(FIRSTNAME_TEXTFIELD, updateFirstName);
    }

    public void updateLastName(String updateLastName) {
        waitForElementVisible(LASTNAME_TEXTFIELD);
        clearText(LASTNAME_TEXTFIELD);
        input(LASTNAME_TEXTFIELD, updateLastName);
    }

    public void updateDoB(String day, String month, String years) {
        selectValueInDropdown(DAY_OPTION, day);
        selectValueInDropdown(MONTH_OPTION, month);
        selectValueInDropdown(YEAR_OPTION, years);
    }

    public void clickSave() {
        click(SAVE_BUTTON);
    }

    public void verifyDataIsUpdated(String updateFirstName, String updateLastName, String day, String month, String years) {
        String actualFirstName = getElementAttribute(FIRSTNAME_TEXTFIELD, "value");
        String actualLastName = getElementAttribute(LASTNAME_TEXTFIELD, "value");
        String actualDay = getSelectedItemInDropdown(DAY_OPTION);
        String actualMonth = getSelectedItemInDropdown(MONTH_OPTION);
        String actualYear = getSelectedItemInDropdown(YEAR_OPTION);

        assertThat(updateFirstName,equalToIgnoringCase(actualFirstName));
        assertThat(updateLastName, equalToIgnoringCase(actualLastName));
        assertThat(day, equalToIgnoringCase(actualDay));
        assertThat(month, equalToIgnoringCase(actualMonth));
        assertThat(years, equalToIgnoringCase(actualYear));
    }

    public void selectAddressMenu() {
        click(ADDRESS_MENU);
    }

    public void clickAddNew() {
        click(ADD_NEW_ADDRESS_BUTTON);
    }

    public void inputNewAddress(String addressfirstName, String addresslastName, String email, String city, String address, String zipcode, String phoneNumber) {
        input(ADDRESS_FIRSTNAME_TEXTFIELD, addressfirstName);
        input(ADDRESS_LASTNAME_TEXTFIELD, addresslastName);
        input(ADDRESS_EMAIL_TEXTFIELD, addresslastName);
        selectValueInDropdown(ADDRESS_COUNTRY_DROPDOWN,"Viet Nam");
        input(CITY_TEXTFIELD,"Ho Chi Minh");
        input(ADDRESS_TEXTFIELD,"Chu Van An Binh Thanh");
        input(ZIPCODE_TEXTFIELD,"70000");
        input(PHONE_TEXTFIELD,"337159253");
    }

    public void clickSaveAddress() {
        click(SAVE_ADDRESS);
    }

    public void clickChangePassword() {
        click(CHANGE_PASSWORD_BUTTON);
    }

    public void inputOldPassword(String oldPassword) {
        input(OLD_PASSWORD_TEXTFIELD, oldPassword);
    }

    public void inputNewPassword(String newPassword) {
        input(NEW_PASSWORD_TEXTFIELD, newPassword);
        input(CONFIRM_PASSWORD_TEXTFIELD, newPassword);
    }

    public void saveNewPassword() {
        click(SAVE_CHANGEPASSWORD_BUTTON);
    }

    public void logOut() {
        click(LOG_OUT_BUTTON);
    }
}
