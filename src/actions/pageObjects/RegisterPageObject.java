package pageObjects;

import commons.BasePage;
import commons.PageGenerator;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import static pageUI.RegisterPageUI.*;

public class RegisterPageObject extends BasePage {
    public RegisterPageObject(WebDriver driver) {
        super(driver);
    }

    public RegisterPageObject selectGender(String value) throws InterruptedException {
        waitForElementVisible(GENDER_LABEL);
        String gender = String.format(GENDER_RADIO_BUTTON, value.toLowerCase());
        click(gender);
        return this;
    }

    public RegisterPageObject inputFirstName(String value) {
        waitForElementVisible(FIRSTNAME_TEXTFIELD);
        input(FIRSTNAME_TEXTFIELD, value);
        return this;
    }

    public RegisterPageObject inputLastName(String value) {
        waitForElementVisible(LASTNAME_TEXTFIELD);
        input(LASTNAME_TEXTFIELD, value);
        return this;
    }

    public RegisterPageObject inputEmail(String value) {
        waitForElementVisible(EMAIL_TEXTFIELD);
        input(EMAIL_TEXTFIELD, value);
        return this;
    }

    public RegisterPageObject inputPassword(String value) {
        waitForElementVisible(PASSWORD_TEXTFIELD);
        input(PASSWORD_TEXTFIELD, value);
        return this;
    }

    public RegisterPageObject inputConfirmPassword(String value) {
        waitForElementVisible(CONFIRM_PASSWORD_TEXTFIELD);
        input(CONFIRM_PASSWORD_TEXTFIELD, value);
        return this;
    }

    public RegisterPageObject clickRegisterButton() {
        click(REGISTER_BUTTON);
        return this;
    }

    public void isUserCreatedSuccess() {
        String successMessage = "Your registration completed";
        sameTextWith(findElement(REGISTER_SUCCESS_TEXT), successMessage);
    }

    public void clickContinueAfterCreated() {
        waitForElementVisible(REGISTER_BUTTON);
        click(REGISTER_BUTTON);
    }

    public RegisterPageObject selectDOB(String day, String month, String year) {
        waitForElementVisible(DOB_LABEL);
        selectValueInDropdown(DOB_DAY_OPTION, day);
        waitForElementClickable(DOB_MONTH_OPTION);
        selectValueInDropdown(DOB_MONTH_OPTION, month);
        selectValueInDropdown(DOB_YEAR_OPTION, year);
        return this;
    }

    public void isErrorMessageDisplay(String error) {
        String errorFormat = String.format(ERROR_MESSAGE, error);
        Assert.assertTrue(findElement(errorFormat).isDisplayed());
    }

    public MyAccountPageObject clickMyAccount() {
        waitForElementVisible(MYACCOUNT_BUTTON);
        click(MYACCOUNT_BUTTON);
        return (MyAccountPageObject) PageGenerator.getPageInstance(getDriver(), "myaccount");
    }
}
