package pageObjects;

import commons.BasePage;
import org.openqa.selenium.WebDriver;

import static pageUI.ProductPageUI.*;

public class ProductPageObject extends BasePage {

    public ProductPageObject(WebDriver driver) {
        super(driver);
    }

    public void clickAddReview() {
        click(ADD_REVIEW_BUTTON);
    }

    public void addReview(String reviewTitle, String reviewDescription) {
        input(REVIEW_TITLE_TEXTFIELD, reviewTitle);
        input(REVIEW_TEXT_TEXTBOX, reviewDescription);
        click(SUBMIT_REVIEW_BUTTON);
    }
}
