package pageObjects;

import commons.BasePage;
import commons.PageGenerator;
import org.openqa.selenium.WebDriver;

import static commons.GlobalConstant.HOME_URL;
import static pageUI.HomePageUI.*;

public class HomePageObject extends BasePage {
    String productDetails = "https://demo.nopcommerce.com/apple-macbook-pro-13-inch";
    public HomePageObject(WebDriver driver) {
        super(driver);
    }

    public void openHomepage() {
        openUrl(HOME_URL);
    }

    public void isHomePageDisplay() {
        isElementDisplayed(HOMEPAGE_LOGO);
    }

    public RegisterPageObject clickRegister() {
        click(REGISTER_BUTTON);
        return (RegisterPageObject) PageGenerator.getPageInstance(getDriver(),"register");
    }

    public LoginPageObject clickLoginButton() {
        click(LOGIN_BUTTON);
        return (LoginPageObject) PageGenerator.getPageInstance(getDriver(),"login");
    }

    public ProductPageObject openSingleProduct() {
        openUrl(productDetails);
        return (ProductPageObject)  PageGenerator.getPageInstance(getDriver(), "product");
    }
}
