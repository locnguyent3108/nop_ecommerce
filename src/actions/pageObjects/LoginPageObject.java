package pageObjects;

import commons.BasePage;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import static pageUI.LoginPageUI.*;

public class LoginPageObject extends BasePage {


    public LoginPageObject(WebDriver driver) {
        super(driver);
    }

    public void clickLogin() {
        waitForElementClickable(LOGIN_BUTTON);
        click(LOGIN_BUTTON);
    }

    public void isEmailErrorDisplay() {
        waitForElementVisible(EMAIL_ERROR);
        Assert.assertTrue(findElement(EMAIL_ERROR).isDisplayed());
    }

    public void isLoginPageDsplay() {
        Assert.assertTrue(findElement(LOGIN_BUTTON).isDisplayed());
//        Thread.sleep(1000);
    }


    public void inputEmail(String value) {
        input(EMAIL_TEXTFIELD, value);
    }

    public void isEmailValidateErrorDisplay() {
        Assert.assertTrue(findElement(EMAIL_VALIDATION_ERROR).isDisplayed());
    }

    public void inputPassword(String password) {
        waitForElementVisible(PASSWORD_TEXTFIELD);
        input(PASSWORD_TEXTFIELD, password);
    }
}
