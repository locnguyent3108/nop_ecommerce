package testHelper;

import com.github.javafaker.Faker;
import org.testng.annotations.DataProvider;

import java.util.Locale;

public class DataHelper {
    private Faker fakeInstance = null;
    @DataProvider(name = "data-register")
    public Object[][] invalidUser(){
        String gender = "Male";
        String firstName = getRandomFirstName();
        String lastName = getRandomLastName();
        String day = "31";
        String month = "August";
        String year = "1992";
        String email = getEmail();
        String companyName = getCompanyName();
        String password = getPassword();

        return new Object[][] {{gender, firstName, lastName,
                                day, month, year, email, companyName, password}};
    }

    @DataProvider(name = "updateInfo")
    public Object[][] updateNewData() {
        String firstName = getRandomFirstName();
        String lastName = getRandomLastName();
        String day = "22";
        String month = "June";
        String year = "1996";
        return new Object[][] {{ firstName, lastName, day, month, year}};
    }

    @DataProvider(name = "address-info")
    public Object[][] newAddress() {
        String addressFirstName = getRandomFirstName();
        String addressLastName = getRandomLastName();
        String email = getEmail();
        String city = getCity();
        String address1 = getAddress();
        String zipcode = getZipcode();
        String phoneNumber = getPhoneNumber();
        return new Object[][]{{
            addressFirstName, addressLastName, email, city, address1, zipcode, phoneNumber
        }};
    }

    public String getRandomFirstName() {
        createInstance();
        return fakeInstance.name().firstName();
    }

    public String getRandomLastName() {
        createInstance();
        return fakeInstance.name().lastName();
    }

    public String getEmail() {
        createInstance();
        long time = System.currentTimeMillis();
        return "test" + time + "@yopmail.com";
    }

    public String getCompanyName() {
        createInstance();
        return fakeInstance.funnyName().name().toLowerCase(new Locale("EN"));
    }

    public String getPassword() {
        return "123456";
    }

    public String getCity(){
        createInstance();
        return fakeInstance.address().cityName();
    }

    public String getAddress() {
        createInstance();
        return fakeInstance.address().fullAddress();
    }

    public String getZipcode() {
        createInstance();
        return fakeInstance.address().zipCode();
    }

    public String getPhoneNumber() {
        createInstance();
        return fakeInstance.phoneNumber().cellPhone();
    }
    public Faker createInstance() {
        if(fakeInstance == null) {
            return fakeInstance = new Faker();
        } else {
            return fakeInstance;
        }
    }


}
