package listener;


import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

public class TestListener implements ITestListener {
    @Override
    public void onTestStart(ITestResult result) {
        Log.info(result.getMethod().getMethodName() + " Start");
    }

    @Override
    public void onStart(ITestContext context) {
        // not implemented
    }

    @Override
    public void onTestSuccess(ITestResult result) {
        Log.info(result.getMethod().getMethodName() + " PASSED");
    }

    @Override
    public void onTestFailure(ITestResult result) {
        Log.info(result.getMethod().getMethodName() + " FAILED");
        Log.error("FAILED BECAUSE: " + result.getThrowable().getMessage());
    }

}
