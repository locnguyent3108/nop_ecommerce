package pageUI;

public class RegisterPageUI {
    public static final String REGISTER_LABEL = "//h1[contains(text(),'Register')]";
    public static final String GENDER_RADIO_BUTTON = "//input[@id='gender-%s']";
    public static final String FIRSTNAME_LABEL = "//label[contains(text(),'First name:')]";
    public static final String FIRSTNAME_TEXTFIELD = "//input[@id='FirstName']";
    public static final String LASTNAME_TEXTFIELD = "//input[@id='LastName']";
    public static final String EMAIL_TEXTFIELD = "//input[@id='Email']";
    public static final String PASSWORD_TEXTFIELD = "//input[@id='Password']";
    public static final String CONFIRM_PASSWORD_TEXTFIELD = "//input[@id='ConfirmPassword']";
    public static final String REGISTER_BUTTON = "//input[@id='register-button']";
    public static final String REGISTER_SUCCESS_TEXT = "//div[@class='result']";
    public static final String DOB_LABEL = "//label[contains(text(),'Date of birth:')]";
    public static final String DOB_DAY_OPTION = "//select[@name='DateOfBirthDay']";
    public static final String DOB_MONTH_OPTION = "//select[@name='DateOfBirthMonth']";
    public static final String DOB_YEAR_OPTION = "//select[@name='DateOfBirthYear']";

    public static final String GENDER_LABEL = "//label[contains(text(),'Gender:')]";

    //DYNAMIC ERROR MESSAGE
    public static final String ERROR_MESSAGE = "//*[contains(text(),'%s')]";

    //
    public static final String MYACCOUNT_BUTTON = "//a[@class='ico-account']";

}
