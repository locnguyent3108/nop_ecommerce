package pageUI;

public class ProductPageUI {
    public static final String ADD_REVIEW_BUTTON = "//div[@class='product-review-links']/a[text()='Add your review']";
    public static final String REVIEW_TITLE_TEXTFIELD = "//input[@id='AddProductReview_Title']";
    public static final String REVIEW_TEXT_TEXTBOX = "//label[contains(text(),'Review text:')]/following-sibling::textarea";
    public static final String SUBMIT_REVIEW_BUTTON = "//input[@name='add-review']";

}
