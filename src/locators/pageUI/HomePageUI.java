package pageUI;

public class HomePageUI {
    public static final String HOMEPAGE_LOGO = "//div[@class='header-logo']";
    public static final String REGISTER_BUTTON = "//a[@class='ico-register']";
    public static final String LOGIN_BUTTON = "//a[@class='ico-login']";
}
