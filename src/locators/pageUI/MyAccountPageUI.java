package pageUI;

public class MyAccountPageUI {
    public static final String FIRSTNAME_TEXTFIELD = "//input[@id='FirstName']";
    public static final String LASTNAME_TEXTFIELD = "//input[@id='LastName']";
    public static final String DAY_OPTION = "//select[@name='DateOfBirthDay']";
    public static final String MONTH_OPTION = "//select[@name='DateOfBirthMonth']";
    public static final String YEAR_OPTION = "//select[@name='DateOfBirthYear']";
    public static final String SAVE_BUTTON = "//input[@id='save-info-button']";
    public static final String ADDRESS_MENU = "//li[@class='customer-addresses inactive']/a[text()='Addresses']";
    public static final String ADD_NEW_ADDRESS_BUTTON = "//input[@class='button-1 add-address-button']";

    //ADDRESS INFO
    public static final String ADDRESS_FIRSTNAME_TEXTFIELD = "//input[@id='Address_FirstName']";
    public static final String ADDRESS_LASTNAME_TEXTFIELD = "//input[@id='Address_LastName']";
    public static final String ADDRESS_EMAIL_TEXTFIELD = "//input[@id='Address_Email']";
    public static final String ADDRESS_COUNTRY_DROPDOWN = "//select[@id='Address_CountryId']";
    public static final String ADDRESS_TEXTFIELD = "//input[@id='Address_Address1']";
    public static final String CITY_TEXTFIELD = "//input[@id='Address_City']";
    public static final String ZIPCODE_TEXTFIELD = "//input[@id='Address_ZipPostalCode']";
    public static final String PHONE_TEXTFIELD = "//input[@id='Address_PhoneNumber']";
    public static final String SAVE_ADDRESS = "//input[@class='button-1 save-address-button']";
    public static final String CHANGE_PASSWORD_BUTTON = "//li[@class='change-password inactive']/a";
    public static final String OLD_PASSWORD_TEXTFIELD = "//input[@id='OldPassword']";
    public static final String NEW_PASSWORD_TEXTFIELD = "//input[@id='NewPassword']";
    public static final String CONFIRM_PASSWORD_TEXTFIELD = "//input[@id='ConfirmNewPassword']";
    public static final String SAVE_CHANGEPASSWORD_BUTTON = "//input[@class='button-1 change-password-button']";
    public static final String LOG_OUT_BUTTON = "//a[@class='ico-logout']";
}
