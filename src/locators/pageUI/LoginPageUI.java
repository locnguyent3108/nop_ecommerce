package pageUI;

public class LoginPageUI {
    public static final String LOGIN_BUTTON = "//input[@class='button-1 login-button']";
    public static final String EMAIL_ERROR = "//span[@id='Email-error']";
    public static final String EMAIL_TEXTFIELD = "//input[@id='Email']";
    public static final String PASSWORD_TEXTFIELD = "//input[@id='Password']";
    public static final String EMAIL_VALIDATION_ERROR = "//span[@class='field-validation-error']";
}
