package register;

import commons.BaseTest;
import commons.PageGenerator;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.*;
import pageObjects.HomePageObject;
import pageObjects.RegisterPageObject;
import testHelper.DataHelper;

public class TC_UI_Register extends BaseTest {
    WebDriver driver;
    HomePageObject homePageObject;
    RegisterPageObject registerPageObject;
    @BeforeMethod
    public void precondition() {
        driver = getDriver();
        homePageObject = PageGenerator.getHomePage(driver);
        homePageObject.openHomepage();
        homePageObject.isHomePageDisplay();
    }

    @Test
    public void registerWithEmptyData() {
        registerPageObject = homePageObject.clickRegister();
        registerPageObject.clickRegisterButton();
    }

    @Test (dataProvider = "data-register", dataProviderClass = DataHelper.class)
    public void registerWithInvalidData(String gender, String firstName, String lastName,
                                        String day, String month, String year,
                                        String email, String company, String password ) throws InterruptedException {
        registerPageObject = homePageObject.clickRegister();
        registerPageObject.waitForPageReady();
        registerPageObject.
                selectGender(gender)
                .inputFirstName(firstName)
                .inputLastName(lastName)
                .selectDOB(day, month, year)
                .inputEmail(email)
                .inputPassword(password)
                .inputConfirmPassword(password)
                .clickRegisterButton();
        registerPageObject.isUserCreatedSuccess();
    }

    @Test
    public void passwordLessThen6Chars() throws InterruptedException {
        String password = "123";
        registerPageObject = homePageObject.clickRegister();
        registerPageObject.waitForPageReady();
        registerPageObject
                .inputPassword(password)
                .inputConfirmPassword(password)
                .clickRegisterButton();
        registerPageObject.isErrorMessageDisplay("must have at least 6 characters");
    }

    @Test
    public void passwordMisMatch() throws InterruptedException {
        String password = "12356";
        String confirmPassword = "1234";
        registerPageObject = homePageObject.clickRegister();
        registerPageObject.waitForPageReady();
        registerPageObject
                .inputPassword(password)
                .inputConfirmPassword(confirmPassword)
                .clickRegisterButton();
        registerPageObject.isErrorMessageDisplay("The password and confirmation password do not match.");
    }

    @Test
    public void validRegister() throws InterruptedException {

    }
}
