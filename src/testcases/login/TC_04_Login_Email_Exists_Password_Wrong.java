package login;

import commons.BaseTest;
import commons.PageGenerator;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pageObjects.HomePageObject;
import pageObjects.LoginPageObject;

public class TC_04_Login_Email_Exists_Password_Wrong extends BaseTest {
    WebDriver driver;
    HomePageObject homePageObject;
    LoginPageObject loginPageObject;

    @BeforeMethod
    public void prepareTest() {
        driver = getDriver();
        homePageObject = PageGenerator.getHomePage(driver);
        homePageObject.openHomepage();
        homePageObject.isHomePageDisplay();
    }

    //TODO
    @Test
    public void executeTest() throws InterruptedException {
        loginPageObject = homePageObject.clickLoginButton();
        loginPageObject.isLoginPageDsplay();
        loginPageObject.inputEmail("automationfc.vn@gmail.com");
        loginPageObject.inputPassword("123");
        loginPageObject.clickLogin();
    }
}
