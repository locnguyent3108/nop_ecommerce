package login;
import commons.BaseTest;
import commons.PageGenerator;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.*;
import pageObjects.HomePageObject;
import pageObjects.LoginPageObject;

import static pageUI.LoginPageUI.EMAIL_ERROR;

public class TC_01_Login_Empty_Data extends BaseTest{
    WebDriver driver;
    HomePageObject homePageObject;
    LoginPageObject loginPageObject;

    @BeforeMethod
    public void prepareTest() {
        driver = getDriver();
        homePageObject = PageGenerator.getHomePage(driver);
        homePageObject.openHomepage();
        homePageObject.isHomePageDisplay();
    }

    @Test
    public void executeTest() throws InterruptedException {
        loginPageObject = homePageObject.clickLoginButton();
        loginPageObject.clickLogin();
        loginPageObject.isEmailErrorDisplay()
        ;
    }
}
