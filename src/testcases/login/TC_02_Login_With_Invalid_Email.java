package login;

import commons.BaseTest;
import commons.PageGenerator;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pageObjects.HomePageObject;
import pageObjects.LoginPageObject;

public class TC_02_Login_With_Invalid_Email extends BaseTest {
    WebDriver driver;
    HomePageObject homePageObject;
    LoginPageObject loginPageObject;

    @BeforeMethod
    public void prepareTest() {
        driver = getDriver();
        homePageObject = PageGenerator.getHomePage(driver);
        homePageObject.openHomepage();
        homePageObject.isHomePageDisplay();
    }

    @Test
    public void executeTest()   {
        loginPageObject = homePageObject.clickLoginButton();
        loginPageObject.isLoginPageDsplay();
        loginPageObject.inputEmail("abc");
        loginPageObject.clickLogin();
        loginPageObject.isEmailValidateErrorDisplay();
    }
}
