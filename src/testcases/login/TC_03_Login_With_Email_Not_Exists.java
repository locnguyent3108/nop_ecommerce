package login;

import commons.BaseTest;
import commons.PageGenerator;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pageObjects.HomePageObject;
import pageObjects.LoginPageObject;

public class TC_03_Login_With_Email_Not_Exists extends BaseTest {
    WebDriver driver;
    HomePageObject homePageObject;
    LoginPageObject loginPageObject;

    @BeforeMethod
    public void prepareTest() {
        driver = getDriver();
        homePageObject = PageGenerator.getHomePage(driver);
        homePageObject.openHomepage();
        homePageObject.isHomePageDisplay();
    }

    //TODO
    @Test
    public void executeTest() throws InterruptedException {
        loginPageObject = homePageObject.clickLoginButton();
        loginPageObject.isLoginPageDsplay();
        loginPageObject.inputEmail("abc");
        loginPageObject.clickLogin();
        loginPageObject.isEmailValidateErrorDisplay();
    }
}
