package MyAccount;

import com.sun.istack.internal.NotNull;
import commons.BaseTest;
import commons.PageGenerator;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pageObjects.HomePageObject;
import pageObjects.ProductPageObject;
import pageObjects.RegisterPageObject;
import testHelper.DataHelper;

public class TC_04_Review_Product extends BaseTest {
    WebDriver driver;
    String email;
    String password;
    HomePageObject homePageInstance;
    RegisterPageObject registerInstance;
    ProductPageObject productInstance;
    @BeforeMethod
    public void prepareTest() throws InterruptedException {
        driver = getDriver();
        homePageInstance = PageGenerator.getHomePage(driver);
        homePageInstance.openHomepage();
        registerInstance = homePageInstance.clickRegister();
        createNewUser(registerInstance);
    }

    @Test
    public void execute() {
        productInstance = homePageInstance.openSingleProduct();
        productInstance.clickAddReview();
        productInstance.addReview("Text Title", "String abadadl kalj alsdkj a");
    }
    public void createNewUser(@NotNull RegisterPageObject registerInstance) throws InterruptedException {

        DataHelper fakeData = new DataHelper();
        email = fakeData.getEmail();
        password = fakeData.getPassword();

        registerInstance.waitForPageReady();
        registerInstance.
                selectGender("male")
                .inputFirstName(fakeData.getRandomFirstName())
                .inputLastName(fakeData.getRandomLastName())
                .selectDOB("12", "August", "1992")
                .inputEmail(fakeData.getEmail())
                .inputPassword(password)
                .inputConfirmPassword(password)
                .clickRegisterButton();
    }

}
