package MyAccount;

import com.sun.istack.internal.NotNull;
import commons.BaseTest;
import commons.PageGenerator;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pageObjects.HomePageObject;
import pageObjects.LoginPageObject;
import pageObjects.MyAccountPageObject;
import pageObjects.RegisterPageObject;
import testHelper.DataHelper;

import java.util.*;

public class TC_01_Verify_New_Info extends BaseTest {
    WebDriver driver;
    HomePageObject homePageInstance;
    RegisterPageObject registerPageInstance;
    MyAccountPageObject myAccountInstance;
    String email;
    String password;
    @BeforeMethod
    public void initProject() throws InterruptedException {
        driver = getDriver();
        homePageInstance = PageGenerator.getHomePage(driver);
        homePageInstance.openHomepage();
        registerPageInstance = homePageInstance.clickRegister();
        createNewUser(registerPageInstance);
    }

    @Test(dataProvider = "updateInfo", dataProviderClass = DataHelper.class)
    public void execute(String updateFirstName, String updateLastName, String day, String month, String years) {
        myAccountInstance = registerPageInstance.clickMyAccount();
        myAccountInstance.updateFirstName(updateFirstName);
        myAccountInstance.updateLastName(updateLastName);
        myAccountInstance.updateDoB(day, month, years);
        myAccountInstance.clickSave();

        myAccountInstance.verifyDataIsUpdated(updateFirstName, updateLastName, day, month, years);
    }

    public void createNewUser(@NotNull RegisterPageObject registerInstance) throws InterruptedException {

        DataHelper fakeData = new DataHelper();
        email = fakeData.getEmail();
        password = fakeData.getPassword();

        registerInstance.waitForPageReady();
        registerInstance.
                selectGender("male")
                .inputFirstName(fakeData.getRandomFirstName())
                .inputLastName(fakeData.getRandomLastName())
                .selectDOB("12", "August", "1992")
                .inputEmail(fakeData.getEmail())
                .inputPassword(password)
                .inputConfirmPassword(password)
                .clickRegisterButton();
    }

}
