package MyAccount;

import com.sun.istack.internal.NotNull;
import commons.BaseTest;
import commons.PageGenerator;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pageObjects.HomePageObject;
import pageObjects.MyAccountPageObject;
import pageObjects.RegisterPageObject;
import testHelper.DataHelper;

public class TC_02_Update_info extends BaseTest {
    WebDriver driver;
    HomePageObject homePageInstance;
    RegisterPageObject registerPageInstance;
    MyAccountPageObject myAccountPageInstance;
    String email;
    String password;
    @BeforeMethod
    public void initProject() throws InterruptedException {
        driver = getDriver();
        homePageInstance = PageGenerator.getHomePage(driver);
        homePageInstance.openHomepage();
        registerPageInstance = homePageInstance.clickRegister();
        createNewUser(registerPageInstance);
    }

    @Test(dataProvider = "address-info", dataProviderClass = DataHelper.class)
    public void executeTest(String addressfirstName, String addresslastName, String email,
                            String city, String address, String zipcode, String phoneNumber) {
        myAccountPageInstance = registerPageInstance.clickMyAccount();
        myAccountPageInstance.selectAddressMenu();
        myAccountPageInstance.clickAddNew();
        myAccountPageInstance.inputNewAddress(addressfirstName, addresslastName, email, city, address, zipcode, phoneNumber);
        myAccountPageInstance.clickSaveAddress();
    }

    public void createNewUser(@NotNull RegisterPageObject registerInstance) throws InterruptedException {

        DataHelper fakeData = new DataHelper();
        email = fakeData.getEmail();
        password = fakeData.getPassword();

        registerInstance.waitForPageReady();
        registerInstance.
                selectGender("male")
                .inputFirstName(fakeData.getRandomFirstName())
                .inputLastName(fakeData.getRandomLastName())
                .selectDOB("12", "August", "1992")
                .inputEmail(fakeData.getEmail())
                .inputPassword(password)
                .inputConfirmPassword(password)
                .clickRegisterButton();
    }

}
