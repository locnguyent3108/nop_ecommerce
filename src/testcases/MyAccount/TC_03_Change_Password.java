package MyAccount;

import com.sun.istack.internal.NotNull;
import commons.BaseTest;
import commons.PageGenerator;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pageObjects.HomePageObject;
import pageObjects.LoginPageObject;
import pageObjects.MyAccountPageObject;
import pageObjects.RegisterPageObject;
import testHelper.DataHelper;

public class TC_03_Change_Password extends BaseTest {
    WebDriver driver;
    HomePageObject homePageInstance;
    RegisterPageObject registerPageInstance;
    MyAccountPageObject myAccountPageInstance;
    String email;
    String password;
    String newPassword = "123456789";

    @BeforeMethod
    public void initProject() throws InterruptedException {
        driver = getDriver();
        homePageInstance = PageGenerator.getHomePage(driver);
        homePageInstance.openHomepage();
        registerPageInstance = homePageInstance.clickRegister();
        createNewUser(registerPageInstance);
    }

    @Test
    public void executeTest() {
        myAccountPageInstance = registerPageInstance.clickMyAccount();
        myAccountPageInstance.clickChangePassword();
        myAccountPageInstance.inputOldPassword(password);
        myAccountPageInstance.inputNewPassword(newPassword);
        myAccountPageInstance.saveNewPassword();
        myAccountPageInstance.logOut();
        LoginPageObject loginPageObject = homePageInstance.clickLoginButton();
        loginPageObject.inputEmail(email);
        loginPageObject.inputPassword(newPassword);
    }

    public void createNewUser(@NotNull RegisterPageObject registerInstance) throws InterruptedException {

        DataHelper fakeData = new DataHelper();
        email = fakeData.getEmail();
        password = fakeData.getPassword();

        registerInstance.waitForPageReady();
        registerInstance.
                selectGender("male")
                .inputFirstName(fakeData.getRandomFirstName())
                .inputLastName(fakeData.getRandomLastName())
                .selectDOB("12", "August", "1992")
                .inputEmail(fakeData.getEmail())
                .inputPassword(password)
                .inputConfirmPassword(password)
                .clickRegisterButton();
    }
}
